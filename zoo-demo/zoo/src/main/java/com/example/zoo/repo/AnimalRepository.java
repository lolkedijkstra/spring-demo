package com.example.zoo.repo;

import java.util.Collection;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.zoo.domain.Animal;

public interface AnimalRepository extends JpaRepository<Animal, String> {
	Optional<Animal> findByName(String name);
	Collection<Animal> findByZooId(Long id);
}
