package com.example.zoo.service;

import java.util.Collection;

import com.example.zoo.domain.Animal;

public interface AnimalService {
	public boolean addAnimal(Animal a);
	public boolean putAnimal(String name, Animal a);
	public Animal getAnimal(Long zoo, String name);
	public Collection<Animal> getAnimals();
	public Collection<Animal> getAnimalsByZooId(Long id);
}
