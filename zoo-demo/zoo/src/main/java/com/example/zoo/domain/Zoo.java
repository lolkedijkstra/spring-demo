package com.example.zoo.domain;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
public class Zoo {

	@Id
	@GeneratedValue
	@Column(unique=true)
	private Long id;
	
	@NotEmpty
	@Column(unique=true)
	private String name;
	
	@OneToMany (mappedBy="zoo")
	private List<Animal> animalList;
	Zoo(){/*jpa*/}
	
	public final List<Animal> getAnimals() {
		return animalList;
	}

	public Zoo(String name) {
		this.name = name;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	
}
