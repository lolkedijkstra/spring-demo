package com.example.zoo.service;

import java.util.Collection;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.example.zoo.domain.Zoo;
import com.example.zoo.repo.ZooRepository;

@Service(value="zooService")
public class ZooServiceImpl implements ZooService {
	private static Logger logger = Logger.getLogger(ZooServiceImpl.class);
	
	private ZooRepository zooStorage = null;

	public ZooServiceImpl(ZooRepository zooStorage) {
		logger.info("initializing");
		this.zooStorage = zooStorage;
	}

	@Override
	public boolean addZoo(Zoo zoo) {
		if (!zooStorage.findByName(zoo.getName()).isPresent()) {
			zooStorage.save(zoo);
			return true;
		}
		return false;
	}	

	@Override
	public Zoo getZoo(Long id) {
		Optional<Zoo> zoo = zooStorage.findById(id);
		return zoo.isPresent() ? zoo.get() : null;
	}
	
	
	@Override
	public Zoo getZooByName(String name) {
		Optional<Zoo> zoo = zooStorage.findByName(name);
		return zoo.isPresent() ? zoo.get() : null;
	}


	@Override
	public boolean putZoo(Long id, Zoo zoo) {
		Optional<Zoo> z = zooStorage.findById(id);
		if (!z.isPresent()) 
			return false;
		
		zoo.setId(id);
		zooStorage.save(zoo);
		return true;
	}


	@Override
	public Collection<Zoo> getZoos() {
		return zooStorage.findAll();
	}

}
