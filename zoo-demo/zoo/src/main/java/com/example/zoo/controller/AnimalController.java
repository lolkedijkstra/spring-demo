package com.example.zoo.controller;

import java.net.URI;
import java.util.Collection;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.example.zoo.domain.Animal;
import com.example.zoo.domain.Zoo;
import com.example.zoo.service.AnimalService;
import com.example.zoo.service.ZooService;


import static com.example.zoo.controller.Msg.getMsg;

@RestController
@RequestMapping("/zoos/{id}/animals")
public class AnimalController {
	private static Logger logger = Logger.getLogger(AnimalController.class);
	
	private AnimalService animalService; 
	private ZooService zooService; 

	
	@Autowired
	public AnimalController(ZooService zooService, AnimalService animalService) {
		logger.info("initializing");
		this.zooService = zooService;
		this.animalService = animalService;
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<Collection<Animal>> getAnimals(@PathVariable Long id) {
		Collection<Animal> animals = this.animalService.getAnimalsByZooId(id);
		
		return (animals.isEmpty())
				? new ResponseEntity<Collection<Animal>>(HttpStatus.NO_CONTENT)
				: new ResponseEntity<Collection<Animal>>(animals, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/{name}")
	public ResponseEntity<?> get(@PathVariable Long id, @PathVariable String name) {
		logger.info("AnimalController.get");
		Zoo z = zooService.getZoo(id);
		if( null == z )
			return new ResponseEntity<String>(getMsg(Msg.E03, id.toString()), HttpStatus.NOT_FOUND);
		
		Animal a = animalService.getAnimal(id, name);
		if ( null == a ) 			
			return new ResponseEntity<String>(getMsg(Msg.E04, name), HttpStatus.NOT_FOUND);

		return new ResponseEntity<Animal>(a, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<?> add(@PathVariable Long id, @RequestBody Animal a) {	
		logger.info("AnimalController.add");
		Zoo z = zooService.getZoo(id);
		if (null == z)
			return new ResponseEntity<String>(getMsg(Msg.E03, id.toString()), HttpStatus.NOT_FOUND);
			
		a.setZoo(z);
		if (!animalService.addAnimal(a))
			return new ResponseEntity<String>(getMsg(Msg.E01, a.getName()), HttpStatus.METHOD_NOT_ALLOWED);

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{name}").buildAndExpand(a.getName())
				.toUri();
		logger.info(location);
		return new ResponseEntity<String>(getMsg(Msg.M01, a.getName()), HttpStatus.CREATED);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{name}")
	public ResponseEntity<?> put(@PathVariable Long id, @PathVariable String name, @RequestBody Animal a) {	
		Zoo z = zooService.getZoo(id);
		if (null == z)
			return new ResponseEntity<String>(getMsg(Msg.E03, id.toString()), HttpStatus.NOT_FOUND);
		
		a.setZoo(z);
		if (!animalService.putAnimal(name, a))
			return new ResponseEntity<String>(getMsg(Msg.E02, a.getName()), HttpStatus.METHOD_NOT_ALLOWED);

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{name}").buildAndExpand(a.getName())
				.toUri();
		logger.info(location);
		return new ResponseEntity<String>(getMsg(Msg.M02, a.getName()), HttpStatus.ACCEPTED);
	}
}
