package com.example.zoo.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;

 
public class CustomBasicAuthenticationEntryPoint extends BasicAuthenticationEntryPoint {
	private static Logger logger = Logger.getLogger(CustomBasicAuthenticationEntryPoint.class);
	
    @Override
    public void commence(final HttpServletRequest request, 
            final HttpServletResponse response, 
            final AuthenticationException authException) throws IOException, ServletException {
    	logger.info("CustomBasicAuthenticationEntryPoint.commence");
    	
    	// --- authentication failed, send error response.
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        response.addHeader("WWW-Authenticate", "Basic realm=" + getRealmName() + "");
         
        response.getWriter().println("HTTP Status 401 : " + authException.getMessage());
    }
     
    @Override
    public void afterPropertiesSet() throws Exception {
    	logger.info("CustomBasicAuthenticationEntryPoint.afterPropertiesSet");
        setRealmName(SecurityConfiguration.REALM);
        super.afterPropertiesSet();
    }
}