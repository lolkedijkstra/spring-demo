package com.example.zoo.domain;

public enum SexType {
	MALE,
	FEMALE
}
