package com.example.zoo.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;


import com.example.zoo.domain.Zoo;

public interface ZooRepository extends JpaRepository<Zoo, Long> {
	Optional<Zoo> findById(Long id);
	Optional<Zoo> findByName(String name);
}
