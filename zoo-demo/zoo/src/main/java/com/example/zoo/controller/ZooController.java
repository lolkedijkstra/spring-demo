package com.example.zoo.controller;

import java.net.URI;
import java.util.Collection;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.example.zoo.domain.Zoo;
import com.example.zoo.service.ZooService;

import static com.example.zoo.controller.Msg.getMsg;

@RestController
@RequestMapping("/zoos")
public class ZooController {
	private static Logger logger = Logger.getLogger(ZooController.class);
	
	private ZooService zooService; 

	
	@Autowired
	public ZooController(ZooService zooService) {
		logger.info("initializing");
		this.zooService = zooService;
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<Collection<Zoo>> getZoos() {
		Collection<Zoo> zoos = this.zooService.getZoos();
		
		return (zoos.isEmpty())
			? new ResponseEntity<Collection<Zoo>>(HttpStatus.NO_CONTENT)
			: new ResponseEntity<Collection<Zoo>>(zoos, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public ResponseEntity<?> get(@PathVariable Long id) {
		logger.info("ZooController.get");
		Zoo z = zooService.getZoo(id);
		return null == z 
			? new ResponseEntity<String>(getMsg(Msg.E04, id.toString()), HttpStatus.NOT_FOUND)
			: new ResponseEntity<Zoo>(z, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<?> add(@RequestBody Zoo zoo) {	
		logger.info("ZooController.add");
		if (!zooService.addZoo(zoo))
			return new ResponseEntity<String>(getMsg(Msg.E01, zoo.getName()), HttpStatus.METHOD_NOT_ALLOWED);

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().buildAndExpand().toUri();
		logger.info(location);
		return new ResponseEntity<String>(getMsg(Msg.M01, zoo.getName()), HttpStatus.CREATED);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{id}")
	public ResponseEntity<?> put(@PathVariable("id") Long id, @RequestBody Zoo zoo) {	
		try {
			if (!zooService.putZoo(id, zoo))
				return new ResponseEntity<String>(getMsg(Msg.E02, id.toString()), HttpStatus.METHOD_NOT_ALLOWED);

			URI location = ServletUriComponentsBuilder.fromCurrentRequest().buildAndExpand(id).toUri();
			logger.info(location);
			return new ResponseEntity<String>(getMsg(Msg.M02, id.toString()), HttpStatus.ACCEPTED);
		} catch (DataIntegrityViolationException e) {
			logger.error(e.getMessage());
			return new ResponseEntity<String>(getMsg(Msg.E06, zoo.getName()), HttpStatus.METHOD_NOT_ALLOWED);
		}
	}

}
