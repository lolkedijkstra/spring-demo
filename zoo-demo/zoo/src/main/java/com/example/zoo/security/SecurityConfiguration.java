package com.example.zoo.security;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

/*
 * http://websystique.com/spring-security/secure-spring-rest-api-using-basic-authentication/
 */


@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
	private static Logger logger = Logger.getLogger(SecurityConfiguration.class);

	// --- roles
	static final String ADMIN = "ADMIN";
	static final String USER = "USER";

	// --- user
	static class User {
		final private String username;
		final private String password;
		// --- semicolon separated list of roles
		final private String roles;

		User(String user, String pw, String roles) {
			this.username = user;
			this.password = pw;
			this.roles = roles;
		}
	};

	// --- define users
	final User USERS[] = { 
			new User("admin", "admin", ADMIN + ";" + USER), 
			new User("lolke", "dijkstra", USER) 
	};

	// --- define realm
	static final String REALM = "ZooAppication";

	@Autowired
	public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception {
		logger.info("configureGlobalSecurity");
		
		for (User u : USERS) {
			logger.info(String.format("username: %s, password: %s, roles: %s", u.username, u.password, u.roles));
			auth.inMemoryAuthentication().withUser(u.username).password(u.password).roles(u.roles.split(";"));
		}
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		logger.info("configure");

		http.csrf().disable()
			.authorizeRequests()
				.antMatchers(HttpMethod.GET,"/api1.0/**").hasRole(USER)
				.antMatchers(HttpMethod.PUT,"/api1.0/**").hasRole(ADMIN)
				.antMatchers(HttpMethod.POST,"/api1.0/**").hasRole(ADMIN)
			.and()
			.httpBasic()		
			.realmName(REALM)
			.authenticationEntryPoint(getBasicAuthEntryPoint())
			.and()
			.sessionManagement()
			.sessionCreationPolicy(SessionCreationPolicy.STATELESS);
	}

	@Bean
	public CustomBasicAuthenticationEntryPoint getBasicAuthEntryPoint() {
		logger.info("getBasicAuthEntryPoint");
		return new CustomBasicAuthenticationEntryPoint();
	}

	/* To allow Pre-flight [OPTIONS] request from browser */
	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers(HttpMethod.OPTIONS, "/**");
	}
}