package com.example.zoo;

import java.sql.Date;

//import java.sql.Date;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.context.annotation.Bean;

import com.example.zoo.domain.Animal;
import com.example.zoo.domain.SexType;
import com.example.zoo.domain.Zoo;
import com.example.zoo.repo.AnimalRepository;
import com.example.zoo.repo.ZooRepository;

@SpringBootApplication
public class ZooApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZooApplication.class, args);
	}
		
	//--- get some initial data
	@Bean
	public CommandLineRunner initialize(ZooRepository zooRepo, AnimalRepository animalRepo) {
		return (evt) -> {
			Zoo a = zooRepo.save(new Zoo("Artis"));
			animalRepo.save(new Animal(a, "piepje", Date.valueOf("2010-10-10"), SexType.MALE));
			animalRepo.save(new Animal(a, "daffie", Date.valueOf("2002-04-28"), SexType.MALE));

			Zoo b = zooRepo.save(new Zoo("Apenheul"));
			animalRepo.save(new Animal(b, "bongo1", Date.valueOf("2010-10-10"), SexType.MALE));
			animalRepo.save(new Animal(b, "bongo2", Date.valueOf("2002-04-28"), SexType.MALE));	
		};
	}
}
