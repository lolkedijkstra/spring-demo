package com.example.zoo.controller;

import java.util.HashMap;
import java.util.Map;

public enum Msg {

	M01, M02, M03,
	E01, E02, E03, E04, E05, E06;
		
	static final Map<Msg, String> msg = new HashMap<Msg, String>();
	
	static {
		msg.put(M01, "Resource [%s] created");
		msg.put(M02, "Resource [%s] updated");
		msg.put(M03, "Resource [%s] found");
		msg.put(E01, "Cannot add resource. Resource [%s] already exists");
		msg.put(E02, "Cannot modify resource. Resource [%s] does not exists");
		msg.put(E03, "Parent [%s] does not exist");
		msg.put(E04, "Resource [%s] does not exists");
		msg.put(E05, "Cannot modify resource id. Resource with id [%s] already exists");
		msg.put(E06, "Cannot modify resource id. Resource with unique field [%s] already exists");
	}
	
	static final String getMsg(Msg code, String resource) {
		final String fmt = "{\"result\": \"%s\",\"msg\": \"%s\"}";
		return String.format(fmt, code, String.format(msg.get(code), resource));
	}	
}

