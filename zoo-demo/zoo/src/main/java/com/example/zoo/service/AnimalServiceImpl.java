package com.example.zoo.service;

import java.util.Collection;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.zoo.domain.Animal;
import com.example.zoo.repo.AnimalRepository;
import com.example.zoo.repo.ZooRepository;

@Service(value="animalService")
public class AnimalServiceImpl implements AnimalService {
	private static Logger logger = Logger.getLogger(AnimalServiceImpl.class);

	private final AnimalRepository animalStorage;
	private final ZooRepository zooStorage;
	
	@Autowired
	public AnimalServiceImpl(ZooRepository zooStorage, AnimalRepository animalStorage) {
		logger.info("initializing");
		this.zooStorage = zooStorage;
		this.animalStorage = animalStorage;
	}

	@Override
	public boolean addAnimal(Animal a) {
		if (!animalStorage.findByName(a.getName()).isPresent()) {
			animalStorage.save(a);
			return true;
		}
		return false;
	}
	
	@Override
	public boolean putAnimal(String name, Animal a) {
		if (animalStorage.findByName(name).isPresent()) {
			a.setName(name);	// cannot change name here: it is PK
			animalStorage.save(a);
			return true;
		}
		return false;
	}

	@Override
	public Collection<Animal> getAnimals() {
		return animalStorage.findAll();
	}

	public void validateZoo(String name) {
		zooStorage.findByName(name).orElseThrow( () -> new NoSuchElementException(name) );		
	}

	@Override
	public Collection<Animal> getAnimalsByZooId(Long id) {
		return animalStorage.findByZooId(id);
	}

	@Override
	public Animal getAnimal(Long zooId, String name) {
		logger.info(zooId + "/" + name);
		Optional<Animal> r = this.animalStorage.findByName(name);
		if (!r.isPresent())
			return null;
		
		return (zooId.equals(r.get().getZoo().getId()) ) 
			? r.get()
			: null;
	}
}
