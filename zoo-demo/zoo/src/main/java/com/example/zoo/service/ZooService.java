package com.example.zoo.service;

import java.util.Collection;

import com.example.zoo.domain.Zoo;

public interface ZooService {
	public boolean addZoo(Zoo a);
	public boolean putZoo(Long id, Zoo a);
	public Zoo getZoo(Long id);
	public Zoo getZooByName(String name);
	public Collection<Zoo> getZoos();
}
