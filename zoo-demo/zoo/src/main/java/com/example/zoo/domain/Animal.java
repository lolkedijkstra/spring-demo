package com.example.zoo.domain;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.*;

//import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Animal {

	@JsonIgnore
	@ManyToOne
	@NotNull(message = "zoo cannot be null")
	private Zoo zoo;
	
	@Id
	@Pattern(regexp = "[a-z-A-Z0-9]*", message = "supported pattern: [a-z-A-Z0-9]")
	private String name;
	Animal(){/*jpa*/}
	
	@NotNull(message = "birthDate cannot be empty")
    @Past(message = "birthDate must be the past")	
	private Date birthDate;
	

	private SexType sex;
	
	public boolean equals(Object that) {
		if (that == null)
			return false;
		
		if (!(that instanceof Animal))
			return false;
		
		return ((Animal)that).name.equals(this.name);
	}
	
	
	public Zoo getZoo(){
		return this.zoo;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getName() { 
		return this.name;
	}
	public Date getBirthDate() {
		return this.birthDate;
	}
	public final SexType getSex() {
		return this.sex;
	}
	
	public Animal(Zoo zoo, String name, Date birthDate, SexType sex) {
		this.zoo = zoo;
		this.name = name;
		this.birthDate = birthDate;
		this.sex = sex;		
	}

	public void setZoo(Zoo zoo) {
		this.zoo = zoo;		
	}

}
