# README #


### What is this repository for? ###

** Quick summary **

This application is a basic demo application for Spring Boot.
The project setup was generated using Spring starter kit (from within Eclipse).

It uses the following:
- Maven for dependancy management
- Spring MVC for REST
- Spring Security (Basic Auth)
- Spring Validation (JSR303 Bean validation)
- Spring health en metrics

* Version 0.1


### How do I get set up? ###

* Clone the repo and import it into your IDE
* To run the application from the command line, from the project root directory do: mvn package
* Next $ java -jar target/zoo-0.0.1-SNAPSHOT.jar

### API ###
{{base}} should be replaced with localhost:8080/api1.0

** authentication **
* Use basic authentication. Otherwise no access.
* For GET requests use: lolke (username)/ dijkstra (password)
* For PUT and POST use: admin / admin

** Zoos **

GET http://{{base}}/zoos

zoo no 1:

GET http://{{base}}/zoos/1

POST http://{{base}}/zoos


```
#!json

{
	"name": "Beekse-Bergen"
}
```



PUT http://{{base}}/zoos/3 and use JSON body to update the fields

```
#!json

{
	"name": "Beekse Bergen"
}
```



** Within a zoo **

GET http://{{base}}/zoos/1/animals/

GET http://{{base}}/zoos/1/animals/piepje

POST http://{{base}}/zoos/1/animals/
```
#!json

{
	"name": "boris",
	"birthDate": "2010-10-21",
	"sex": "MALE"
}
```

### Further ###

* You may use this as you like.